package com.android.example.roomdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.room.*
import kotlinx.android.synthetic.main.activity_main.*

// Highscore
// id, name, score
// 0, Kirsi Kernel, 1000.00
// 1, Matti Mainio, 900.00

@Entity
data class Highscore(
    @PrimaryKey var id: Int,
    var name: String?,
    var score: Double?
)

@Database(entities = [Highscore::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun hsDao(): HSDao
}

@Dao
interface HSDao {
    @Query("SELECT * FROM Highscore")
    fun loadAll(): Array<Highscore>

    @Insert
    fun insert(hs: Highscore)
}

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // create database and get instance
        val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            "hs_db"
        ).allowMainThreadQueries().build() // allowMainThreadQueries <- avoid this

        // clear
        runOnUiThread { db.clearAllTables() }

        // add
        db.hsDao().insert(Highscore(0, "Kirsi Kernel", 1000.00))
        db.hsDao().insert(Highscore(1, "Matti Mainio", 900.00))

        // get
        val allHS = db.hsDao().loadAll()

        // show
        textView.text = ""
        allHS.forEachIndexed { index, highscore ->
            textView.append("${highscore.name} : ${highscore.score} \n")
        }
    }
}
