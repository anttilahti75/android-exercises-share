package com.android.example.employeetabbeddemo.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.android.example.employeetabbeddemo.R

/**
 * A placeholder fragment containing a simple view.
 */
class PlaceholderFragment : Fragment() {

    private lateinit var pageViewModel: PageViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel::class.java).apply {
            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
            setName(arguments?.getString(ARG_NAME ) ?: "Employee name")
            setImage(arguments?.getString(ARG_IMAGE) ?: "Employee image")
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_main, container, false)

        /*
        val textView: TextView = root.findViewById(R.id.section_label)
        pageViewModel.text.observe(this, Observer<String> {
            textView.text = it
        })
        */

        // Name
        val nameTextView: TextView = root.findViewById(R.id.nameTextView)
        pageViewModel.name.observe(viewLifecycleOwner, Observer<String> {
            nameTextView.text = it
        })

        // Image
        val imageView: ImageView = root.findViewById(R.id.imageView)
        pageViewModel.image.observe(viewLifecycleOwner, Observer<String> {
            // image == "employee01", "employee02", ...
            val id = resources.getIdentifier("com.android.example.employeetabbeddemo:drawable/$it", null, null)
            imageView.setImageResource(id)
        })

        return root
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"
        private const val ARG_NAME = "name"
        private const val ARG_IMAGE = "image"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int, name: String, image: String): PlaceholderFragment {
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                    putString(ARG_NAME, name)
                    putString(ARG_IMAGE, image)
                }
            }
        }
    }
}