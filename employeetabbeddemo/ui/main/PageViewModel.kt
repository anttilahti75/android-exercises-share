package com.android.example.employeetabbeddemo.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PageViewModel : ViewModel() {

    // Hold
    private val _index = MutableLiveData<Int>()
    private val _name = MutableLiveData<String>()
    private val _image = MutableLiveData<String>()

    /*
    val text: LiveData<String> = Transformations.map(_index) {
        "Hello world from section: $it"
    }
    */

    // Get
    val name: LiveData<String> = Transformations.map(_name) { it }

    val image: LiveData<String> = Transformations.map(_image) { it }

    // Set
    fun setIndex(index: Int) {
        _index.value = index
    }

    fun setName(name: String) {
        _name.value = name
    }

    fun setImage(image: String) {
        _image.value = image
    }
}