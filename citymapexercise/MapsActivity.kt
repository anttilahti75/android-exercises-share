package com.android.example.citymapexercise

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val finCenter = LatLng(64.3, 26.7)
        val oulu = LatLng(65.0, 25.4)
        val tampere = LatLng(61.5, 23.8)
        val vaasa = LatLng(63.1, 21.6)
        val jyvaskyla = LatLng(62.2, 25.7)
        val kuopio = LatLng(62.9, 27.7)
        mMap.addMarker(MarkerOptions().position(oulu).title("Marker in Oulu").snippet("Population: 205 629"))
        mMap.addMarker(MarkerOptions().position(tampere).title("Marker in Tampere").snippet("Population: 235,615"))
        mMap.addMarker(MarkerOptions().position(vaasa).title("Marker in Vaasa").snippet("Population: 67 623"))
        mMap.addMarker(MarkerOptions().position(jyvaskyla).title("Marker in Jyväskylä").snippet("Population: 142 477"))
        mMap.addMarker(MarkerOptions().position(kuopio).title("Marker in Kuopio").snippet("Population: 119 379"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(finCenter, 5.0f))
    }
}
