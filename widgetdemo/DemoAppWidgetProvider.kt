package com.android.example.widgetdemo

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews

class DemoAppWidgetProvider : AppWidgetProvider() {

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        // Perform this loop procedure for each App Widget that belongs to this provider
        appWidgetIds.forEach { appWidgetId ->
            // Create an Intent to launch MainActivity
            val pendingIntent: PendingIntent = Intent(context, MainActivity::class.java)
                .let { intent ->
                    PendingIntent.getActivity(context, 0, intent, 0)
                }

            // Get the layout for the App Widget and attach an on-click listener
            // to the button
            val views: RemoteViews = RemoteViews(
                context.packageName,
                R.layout.demo_appwidget
            ).apply {
                setOnClickPendingIntent(R.id.activityButton, pendingIntent)
            }

            val randomIntent = Intent(context, DemoAppWidgetProvider::class.java)
            randomIntent.action = "com.android.example.widgetdemo.RANDOM"
            randomIntent.putExtra("appWidgetId", appWidgetId)

            val randomPendingIntent = PendingIntent.getBroadcast(context, 0, randomIntent, 0)
            views.setOnClickPendingIntent(R.id.randomButton, randomPendingIntent)


            // Tell the AppWidgetManager to perform an update on the current app widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)

        if (intent.action == "com.android.example.widgetdemo.RANDOM") {
            val appWidgetManager = AppWidgetManager.getInstance(context.applicationContext)
            val views = RemoteViews(context.packageName, R.layout.demo_appwidget)
            val appWidgetId = intent.extras!!.getInt("appWidgetId") // Exclamations say that getInt cannot be null

            val random = Math.random()
            views.setTextViewText(R.id.randomTextView, random.toString())

            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }
}