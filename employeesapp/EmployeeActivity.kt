package com.android.example.employeesapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_employee.*
import kotlinx.android.synthetic.main.activity_employee.departmentTextView
import kotlinx.android.synthetic.main.activity_employee.emailTextView
import kotlinx.android.synthetic.main.activity_employee.nameTextView
import kotlinx.android.synthetic.main.activity_employee.phoneTextView
import kotlinx.android.synthetic.main.activity_employee.titleTextView
import kotlinx.android.synthetic.main.employee_item.*
import kotlinx.android.synthetic.main.employee_item.view.*
import org.json.JSONObject

class EmployeeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee)

        // get data from intent
        val bundle: Bundle? = intent.extras;
        if (bundle != null) {
            val employeeString = bundle!!.getString("employee")
            val employee = JSONObject(employeeString)
            val firstName = employee["firstName"]
            val lastName = employee["lastName"]
            val title = employee["title"]
            val email = employee["email"]
            val phone = employee["phone"]
            val department = employee["department"]
            val imageURL = employee["image"]
            val description = "Lorem ipsum dolor sit amet"

            // modify here to display other employee's data too!
            Toast.makeText(this, "Name is $firstName",Toast.LENGTH_LONG).show()
            nameTextView.text = firstName.toString() + " " + lastName.toString()
            titleTextView.text = title.toString()
            emailTextView.text = email.toString()
            phoneTextView.text = phone.toString()
            departmentTextView.text = department.toString()
            descriptionTextView.text = "$description"
            Glide.with(empImageView.context).load(imageURL).into(empImageView)
        }
    }
}
