package com.android.example.locationdemo

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    // Fused Location Provider Client instance
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    // LocationCallback object - Used in Location Updates
    private lateinit var locationCallback: LocationCallback
    // Location Request "dialog" ID
    private val MY_PERMISSIONS_REQUEST_LOCATION = 1
    // Request updates or not
    private var requestingLocationUpdates: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Create an instance of the Fused Location Provider Client
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        // location update callback object
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations){
                    // Update UI with location data
                    if (location != null) {
                        latitudeTextView.text = location.latitude.toString()
                        longitudeTextView.text = location.longitude.toString()
                    }
                }
            }
        }

        // Check location permissions in run time
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                Toast.makeText(this,"App needs a Location to work", Toast.LENGTH_LONG).show()
            } else {
                // Request the permission
                ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_LOCATION)
            }
        }
    }

    // request permissions result
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray)
    {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // Permission was granted
                    Toast.makeText(this,"Location permission granted!", Toast.LENGTH_LONG).show()
                    requestingLocationUpdates = true
                } else {
                    // permission denied
                    Toast.makeText(this,"Location permission denied!", Toast.LENGTH_LONG).show()
                }
                return
            }
        }
    }

    // start location updates
    private fun startLocationUpdates() {
        // Create location request object
        val locationRequest = LocationRequest.create()?.apply {
            interval = 1000
            fastestInterval = 500
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        Toast.makeText(this, "Location updates started!", Toast.LENGTH_LONG).show()
        fusedLocationClient.requestLocationUpdates(locationRequest,
            locationCallback,
            null /* Looper */)
        // The Looper object whose message queue will be used to implement the callback mechanism, or null to make callbacks on the calling thread.

    }

    // stop location updates
    private fun stopLocationUpdates() {
        Toast.makeText(this, "Location updates stopped!", Toast.LENGTH_LONG).show()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    // resume
    override fun onResume() {
        super.onResume()
        if (requestingLocationUpdates) startLocationUpdates()
    }

    // pause
    override fun onPause() {
        super.onPause()
        if (requestingLocationUpdates) stopLocationUpdates()
    }
}
