package com.android.example.justtesting

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView

class Act2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act2_layout)

        // Get the Intent that started this activity and extract the string
        val message = intent.getStringExtra("msg")

        // Capture the layout's TextView and set the string as its text
        val textView = findViewById<TextView>(R.id.act2TextView)
        textView.text = message
    }

}