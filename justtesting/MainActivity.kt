package com.android.example.justtesting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun showMsg(view: View) {
        val text = "Hello toast!"
        val mySnackbar = Snackbar.make(view, text, BaseTransientBottomBar.LENGTH_LONG)
        mySnackbar.show()
    }

    fun chgAct2(view: View) {
        val intent = Intent(this, Act2::class.java).apply {
            putExtra("msg", "msgContent")
        }
        startActivity(intent)
    }

    fun chgAct3(view: View) {
        val intent = Intent(this, Act3::class.java)
        startActivity(intent)
    }

    fun chgMap(view: View) {
        val intent = Intent(this, MapsActivity::class.java)
        startActivity(intent)
    }

}
