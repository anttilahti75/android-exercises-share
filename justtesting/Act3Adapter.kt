package com.android.example.justtesting

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recycler_item.view.*

class Act3Adapter(private val myDataset: Array<String>) :
    RecyclerView.Adapter<Act3Adapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val testTextView: TextView = view.itemTextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Act3Adapter.MyViewHolder {
        val retView = LayoutInflater.from(parent.context).inflate(R.layout.recycler_item, parent, false)
        return MyViewHolder(retView)
    }

    override fun getItemCount(): Int {
        return myDataset.size
    }

    override fun onBindViewHolder(holder: Act3Adapter.MyViewHolder, position: Int) {
        holder.testTextView.text = myDataset[position]
    }


}