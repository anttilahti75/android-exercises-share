package com.android.example.fragmentsdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun openFragmentOne(v: View) {
        val fragOne = FragmentOne()
        supportFragmentManager.beginTransaction().replace(R.id.fragment, fragOne).commit()
    }

    fun openFragmentTwo(v: View) {
        val fragTwo = FragmentTwo()
        supportFragmentManager.beginTransaction().replace(R.id.fragment, fragTwo).commit()
    }
}
