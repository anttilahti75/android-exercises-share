package com.android.example.showgolfcoursesinmapwithclustering

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem


class GolfCourseItem : ClusterItem {
    private var mPosition: LatLng? = null
    private var mTitle: String? = null
    private var mSnippet: String? = null
    private var extraInfo: List<String>? = null

    constructor(lat: Double, lng: Double) {
        mPosition = LatLng(lat, lng)
    }

    fun setExtraInfo(list: List<String>) {
        extraInfo = list
    }

    fun getExtraInfo(): List<String>? {
        return extraInfo
    }

    override fun getPosition(): LatLng? {
        return mPosition
    }

    override fun getTitle(): String? {
        return mTitle
    }

    fun setTitle(title: String) {
        mTitle = title
    }

    override fun getSnippet(): String? {
        return mSnippet
    }

    fun setSnippet(snippet: String) {
        mSnippet = snippet
    }
}