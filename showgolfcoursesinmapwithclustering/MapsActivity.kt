package com.android.example.showgolfcoursesinmapwithclustering

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import kotlinx.android.synthetic.main.course_info_window.view.*
import org.json.JSONArray


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val mClusterManager = ClusterManager<GolfCourseItem>(this, mMap)
        loadCluster(mClusterManager)
    }

    private fun loadCluster(manager: ClusterManager<GolfCourseItem>) {

        val markerClusterRenderer = MarkerClusterRenderer(this, mMap, manager)
        manager.renderer = markerClusterRenderer
        mMap.setInfoWindowAdapter(manager.markerManager)
        manager.markerCollection.setInfoWindowAdapter(GolfCourseInfoWindow())

        mMap.setOnCameraIdleListener(manager)
        loadData(manager)
        mMap.setOnMarkerClickListener(manager)

    }

    private fun loadData(manager: ClusterManager<GolfCourseItem>) {

        val queue = Volley.newRequestQueue(this)
        val url = "https://ptm.fi/materials/golfcourses/golf_courses.json"
        var golfCourses: JSONArray
        val courseTypes: Map<String, Float> = mapOf(
                "?" to BitmapDescriptorFactory.HUE_VIOLET,
                "Etu" to BitmapDescriptorFactory.HUE_BLUE,
                "Kulta" to BitmapDescriptorFactory.HUE_GREEN,
                "Kulta/Etu" to BitmapDescriptorFactory.HUE_YELLOW
        )
        // create JSON request object
        val jsonObjectRequest = JsonObjectRequest(
                Request.Method.GET, url, null,
                Response.Listener { response ->
                    // JSON loaded successfully
                    golfCourses = response.getJSONArray("courses")
                    // loop through all objects
                    for (i in 0 until golfCourses.length()){
                        // get course data
                        val course = golfCourses.getJSONObject(i)
                        val lat = course["lat"].toString().toDouble()
                        val lng = course["lng"].toString().toDouble()
                        val type = course["type"].toString()
                        val title = course["course"].toString()
                        val address = course["address"].toString()
                        val phone = course["phone"].toString()
                        val email = course["email"].toString()
                        val webURL = course["web"].toString()

                        if (courseTypes.containsKey(type)){
                            val tempList: List<String> = listOf(address, phone, email, webURL)
                            val gcItem = GolfCourseItem(lat, lng)
                            gcItem.setTitle(title)
                            gcItem.setExtraInfo(tempList)
                            manager.addItem(gcItem)
                        } else {
                            Log.d("Course", "This course type does not exist in evaluation $type")
                        }
                    }
                    val finland = LatLngBounds(
                        LatLng(59.85, 21.69), LatLng(70.08, 29.24)
                    )
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(finland, 0))
                },
                Response.ErrorListener { error ->
                    // Error loading JSON
                    Log.d("JSON", "Error: $error")
                }
        )
        // Add the request to the RequestQueue
        queue.add(jsonObjectRequest)
    }

    inner class MarkerClusterRenderer(
        context: Context?,
        map: GoogleMap?,
        clusterManager: ClusterManager<GolfCourseItem>?
    ) : DefaultClusterRenderer<GolfCourseItem>(context, map, clusterManager) {

        override fun onClusterItemRendered(clusterItem: GolfCourseItem, marker: Marker) {
            super.onClusterItemRendered(clusterItem, marker)
            marker.tag = clusterItem.getExtraInfo()
        }
    }

    inner class GolfCourseInfoWindow : GoogleMap.InfoWindowAdapter {
        private val contents: View = layoutInflater.inflate(R.layout.course_info_window, null)

        override fun getInfoContents(marker: Marker): View {
            // UI elements
            val titleTextView = contents.titleTextView
            val addressTextView = contents.addressTextView
            val phoneTextView = contents.phoneTextView
            val emailTextView = contents.emailTextView
            val webTextView = contents.webTextView

            // title
            titleTextView.text = marker.title.toString()

            // get data from Tag list

            val list: List<String> = marker.tag as List<String>
            addressTextView.text = list[0]
            phoneTextView.text = list[1]
            emailTextView.text = list[2]
            webTextView.text = list[3]
            return contents
        }

        override fun getInfoWindow(marker: Marker): View? {
            return null
        }

    }
}
