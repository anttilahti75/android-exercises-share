package com.android.example.recycleviewdemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.employee_item.view.*
import org.json.JSONArray

class EmployeesAdapter(private val employees: JSONArray) : RecyclerView.Adapter<EmployeesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeesAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.employee_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val employee = employees.getJSONObject(position)
        // employee name
        //holder.nameTextView.text = employee["lastName"].toString() + " " + employee["firstName"].toString()
        holder.nameTextView.text =
            holder.nameTextView.context.getString(
                R.string.full_name_text,
                employee["lastName"].toString(),
                employee["firstName"].toString()
            )
    }

    override fun getItemCount(): Int {
        return employees.length()
    }

    // ViewHolder
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameTextView: TextView = view.nameTextView
    }
}

