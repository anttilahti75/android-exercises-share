package com.android.example.recycleviewdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView.layoutManager = LinearLayoutManager(this)

        val queue = Volley.newRequestQueue(this)
        val url = "https://www.ptm.fi/data/android_employees.json"

        // listeners
        val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, null,
            Response.Listener {
                response ->
                    val employees = response.getJSONArray("employees")
                    // data to adapter
                    recyclerView.adapter = EmployeesAdapter(employees)
            },
            Response.ErrorListener {
                error -> Log.d("JSON", error.toString())
            }
        )

        // Add the request to the RequestQueue
        queue.add(jsonObjectRequest)
    }
}
