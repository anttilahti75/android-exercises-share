package com.android.example.intentexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        // get data from intent
        val bundle: Bundle? = intent.extras
        if (bundle != null) {
            val name = bundle.getString("name")
            Toast.makeText(this, "Name is $name", Toast.LENGTH_LONG).show()
        }
    }

    fun backButtonPressed(view: View) {
        finish()
    }
}
