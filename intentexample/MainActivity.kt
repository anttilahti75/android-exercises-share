package com.android.example.intentexample

import android.content.Intent
import android.content.pm.ResolveInfo
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun launchActivity(view: View) {
        // start activity
        val intent = Intent(this, SecondActivity::class.java)
        intent.putExtra("name", "Kirsi Kernel")
        startActivity(intent)
    }

    fun launchMap(view: View) {
        // create location with geo protocol (latitude and logitude zero, use address)
        val location = Uri.parse("geo:0,0?q=Piippukatu 2, Jyväskylä, Finland")
        // create intent with action view and use above location
        val mapIntent = Intent(Intent.ACTION_VIEW, location)

        // verify that intent it resolves in device
        val activities: List<ResolveInfo> = packageManager.queryIntentActivities(mapIntent, 0)
        val isIntentSafe: Boolean = activities.isNotEmpty()

        // start an activity if it's safe
        if (isIntentSafe) {
            startActivity(mapIntent)
        } else {
            Toast.makeText(this, "There is no activity to handle map intent!", Toast.LENGTH_LONG).show();
        }
    }
}
