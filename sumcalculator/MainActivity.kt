package com.android.example.sumcalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var num1: String = "0";
    var num2: String = "0";
    var isFirst = true
    var shouldClear = false
    var calcAction: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun calcButtonNumClicked(view: View) {
        if (shouldClear) {
            calcResult.setText("")
            shouldClear = false
        }
        // view is a button (pressed one) get text and convert to Int
        val digit = (view as TextView).text.toString().toInt()

        if (isFirst) {
            num1 += digit
            isFirst = false
        } else {
            num2 += digit
        }

        // append a new string to textView
        calcResult.append(digit.toString())

    }

    fun calcButtonActionClicked(view: View) {
        val action = (view as TextView).text.toString()
        calcAction = action
        calcResult.append(action)
    }

    fun calcButtonCalcResultClicked(view: View) {
        var tNum1 = num1.toInt()
        var tNum2 = num2.toInt()
        var result = doCalcAction(calcAction, tNum1, tNum2)
        calcResult.append(" = " + result)
        num1 = "0"
        num2 = "0"
        isFirst = true
        shouldClear = true
    }

    fun doCalcAction(value: String, num1: Int, num2: Int): String {
        when(value) {
            "+" -> return (num1 + num2).toString()
            "-" -> return (num1 - num2).toString()
            "*" -> return (num1 * num2).toString()
            "/" -> return (num1 / num2).toString()
            else -> {
                return "Error"
            }
        }
    }

}
