package com.android.example.launchamap

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    // show a map with implicit intent
    fun showMap(view: View) {
        // get latitude and longitude values
        val lat = latEditText.text.toString().toDouble()
        val long = longEditText.text.toString().toDouble()

        // build the intent
        val location = Uri.parse("geo:$lat, $long")
        val mapIntent = Intent(Intent.ACTION_VIEW, location)

        // verify that the intent resolves
        val activities = packageManager.queryIntentActivities(mapIntent, 0)
        val isIntentSafe: Boolean = activities.isNotEmpty()

        // start an activity if it is safe
        if (isIntentSafe) {
            startActivity(mapIntent)
        } else {
            Toast.makeText(this, "There is no activity to handle map intent!", Toast.LENGTH_LONG).show()
        }
    }
}
